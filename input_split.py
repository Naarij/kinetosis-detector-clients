from virtual_keyboard_input import *


class InputSplit(object):

    def __init__(self, keys, special_keys = ''):
        self.keys = keys
        self.special_keys = special_keys

    def print(self):
        if len(self.special_keys) > 0:
            print('Tastenkombination: ' + self.special_keys)
        print('Tastendruck: ' + self.keys)
        print('---')

    def perform(self):
        pressed_special_keys = []

        for special_key in self.special_keys:
            if special_key not in pressed_special_keys:
                pressed_special_keys.append(special_key)
                if special_key == 'C':
                    PressKey(VK_CONTROL)
                elif special_key == 'A':
                    PressKey(VK_MENU)
                elif special_key == 'S':
                    PressKey(VK_LSHIFT)

        for key in self.keys:
            hex_code = CharToHex(key)
            PressKey(hex_code)
            time.sleep(1)
            ReleaseKey(hex_code)

        time.sleep(2)

        for special_key in self.special_keys:
            if special_key in pressed_special_keys:
                pressed_special_keys.remove(special_key)
                if special_key == 'C':
                    ReleaseKey(VK_CONTROL)
                elif special_key == 'A':
                    ReleaseKey(VK_MENU)
                elif special_key == 'S':
                    ReleaseKey(VK_LSHIFT)


def read_key_string(input_string):
    input_collector = []

    upper_input_string = input_string.upper()

    while not upper_input_string == '':
        a = upper_input_string.split('<', 1)[0]
        try:
            b = '<' + upper_input_string.split('<', 1)[1]
        except IndexError:
            b = ''
        if not a == '':
            s_i = InputSplit(a)
            input_collector.append(s_i)
        if not b == '':
            c = b.split('>', 1)[1]
            b = b.split('>', 1)[0] + '>'
            try:
                s_i = InputSplit(b.split('-', 1)[1].split('>', 1)[0], b.split('<', 1)[1].split('-', 1)[0])
                input_collector.append(s_i)
                upper_input_string = c
            except IndexError:
                upper_input_string = ''
        else:
            upper_input_string = ''

    return input_collector
