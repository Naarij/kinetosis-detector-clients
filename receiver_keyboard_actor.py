import time
from api_interaction import ApiInteraction
from input_split import read_key_string

api_interaction = ApiInteraction()


def get_pr():
    return api_interaction.send_api_request('get', '/session/prvalue')['current_pr_value']


session = api_interaction.send_api_request('get', '/session')

interaction_string = ''
for i in session['session_interaction_shortcut']:
    interaction_string = interaction_string + i

inputs = read_key_string(interaction_string)

interval = 1
while True:
    time.sleep(interval)
    if get_pr() > session['threshold']:
        for i in inputs:
            i.perform()
        print('I\'m done. Bye!')
        break
